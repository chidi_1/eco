/**
 * appName - http://chidi-frontend.esy.es/
 * @version v0.1.0
 * @author bev-olga@yandex.ru
 */
// анимация первого слайда при загрузке
var start_bg = function () {
    bg_left.removeClass('start');
    bg_right.removeClass('start');
};

var slide_home = function () {
    $('.img-home').removeClass('img-home-start');
    setTimeout(function () {
        $('.img-toilets').removeClass('img-toilets-start');
    }, 300);
    setTimeout(function () {
        $('.slide-down').addClass('bounceIn');
    }, 800);
};

var scheme_animation = function () {
    var inter = 1;
    var timer = 300;

    $('.scheme--toilet').removeClass('scheme--toilet-start');

    var end = +$(".scheme--price .total").data('start');

    $(".scheme--price .total").spincrement({
        from: 0,                // Стартовое число
        to: end,              // Итоговое число. Если false, то число будет браться из элемента с классом spincrement, также сюда можно напрямую прописать число. При этом оно может быть, как целым, так и с плавающей запятой
        decimalPlaces: 0,       // Сколько знаков оставлять после запятой
        decimalPoint: "",      // Разделитель десятичной части числа
        thousandSeparator: "", // Разделитель тыcячных
        duration: 2500          // Продолжительность анимации в миллисекундах
    });

    $('.scheme-list').find('.scheme-list--el').each(function () {
        var block = $(this);
        setInterval(function () {
            block.removeClass('scheme-list--el-start');
        }, inter * timer);
        inter++;
    });
};

var podium_animation = function () {
    setTimeout(function () {
        $('.img-podium').removeClass('img-podium-start');
        $('.price1').removeClass('price1-start');
        $('.price2').removeClass('price2-start');
        $('.price3').removeClass('price3-start');
    }, 500);

    setTimeout(function () {
        var inter = 0;
        var timer = 300;

        $('.podium--el').find('.toilet').each(function () {
            var block = $(this);
            setInterval(function () {
                block.removeClass('toilet-start');

            }, inter * timer);
             inter++
        });
    }, 1000);

    setTimeout(function () {
        $('.podium--el .ref').removeClass('ref-start');
    }, 2000);
};

var parallax_animation = function () {
    /*$('.parallax-layer').parallax({
        mouseport: $(".parallax")
    });*/
};

var promo_animation = function(){
    setTimeout(function () {
        $('.img-man').removeClass('img-man-start');
    }, 500);
    setTimeout(function () {
        $('.parallax-layer').removeClass('parallax-start');
    }, 1000);
}

var start_animation = function () {
    setTimeout(function () {
        $('.slide-start--center').fadeIn(300);
        $('.js--checkout-left').fadeIn(300);
        $('.js--checkout-right').fadeIn(300);
    }, 500);
    setTimeout(function () {
        $('.img-confetti').fadeIn(300);
        $('.toilett-container').removeClass('toilett-container-start');
    }, 1000);
    setTimeout(function () {
        $('.slide-start--left h2').removeClass('title-start');
    }, 1500);
    setTimeout(function () {
        $('.slide-start--right .img-toilet-container').removeClass('toilett-container-start');
        $('.img-crane').addClass('hidden-crane');

    }, 2000);
    setTimeout(function () {
        $('.slide-start--right h2').removeClass('title-start');
    }, 2500);
    setTimeout(function () {
        $('.header').removeClass('header-start');
        $('.global-overlay').removeClass('no-click');
        $('.slide-start--center .wrap').fadeOut(300);
    }, 3000);
};
(function (e) {
    function t() {
        var e = document.createElement("input"), t = "onpaste";
        return e.setAttribute(t, ""), "function" == typeof e[t] ? "paste" : "input"
    }

    var n, a = t() + ".mask", r = navigator.userAgent, i = /iphone/i.test(r), o = /android/i.test(r);
    e.mask = {
        definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"},
        dataName: "rawMaskFn",
        placeholder: "_"
    }, e.fn.extend({
        caret: function (e, t) {
            var n;
            if (0 !== this.length && !this.is(":hidden"))return "number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function () {
                this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && (n = this.createTextRange(), n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
            })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {
                begin: e,
                end: t
            })
        }, unmask: function () {
            return this.trigger("unmask")
        }, mask: function (t, r) {
            var c, l, s, u, f, h;
            return !t && this.length > 0 ? (c = e(this[0]), c.data(e.mask.dataName)()) : (r = e.extend({
                placeholder: e.mask.placeholder,
                completed: null
            }, r), l = e.mask.definitions, s = [], u = h = t.length, f = null, e.each(t.split(""), function (e, t) {
                "?" == t ? (h--, u = e) : l[t] ? (s.push(RegExp(l[t])), null === f && (f = s.length - 1)) : s.push(null)
            }), this.trigger("unmask").each(function () {
                function c(e) {
                    for (; h > ++e && !s[e];);
                    return e
                }

                function d(e) {
                    for (; --e >= 0 && !s[e];);
                    return e
                }

                function m(e, t) {
                    var n, a;
                    if (!(0 > e)) {
                        for (n = e, a = c(t); h > n; n++)if (s[n]) {
                            if (!(h > a && s[n].test(R[a])))break;
                            R[n] = R[a], R[a] = r.placeholder, a = c(a)
                        }
                        b(), x.caret(Math.max(f, e))
                    }
                }

                function p(e) {
                    var t, n, a, i;
                    for (t = e, n = r.placeholder; h > t; t++)if (s[t]) {
                        if (a = c(t), i = R[t], R[t] = n, !(h > a && s[a].test(i)))break;
                        n = i
                    }
                }

                function g(e) {
                    var t, n, a, r = e.which;
                    8 === r || 46 === r || i && 127 === r ? (t = x.caret(), n = t.begin, a = t.end, 0 === a - n && (n = 46 !== r ? d(n) : a = c(n - 1), a = 46 === r ? c(a) : a), k(n, a), m(n, a - 1), e.preventDefault()) : 27 == r && (x.val(S), x.caret(0, y()), e.preventDefault())
                }

                function v(t) {
                    var n, a, i, l = t.which, u = x.caret();
                    t.ctrlKey || t.altKey || t.metaKey || 32 > l || l && (0 !== u.end - u.begin && (k(u.begin, u.end), m(u.begin, u.end - 1)), n = c(u.begin - 1), h > n && (a = String.fromCharCode(l), s[n].test(a) && (p(n), R[n] = a, b(), i = c(n), o ? setTimeout(e.proxy(e.fn.caret, x, i), 0) : x.caret(i), r.completed && i >= h && r.completed.call(x))), t.preventDefault())
                }

                function k(e, t) {
                    var n;
                    for (n = e; t > n && h > n; n++)s[n] && (R[n] = r.placeholder)
                }

                function b() {
                    x.val(R.join(""))
                }

                function y(e) {
                    var t, n, a = x.val(), i = -1;
                    for (t = 0, pos = 0; h > t; t++)if (s[t]) {
                        for (R[t] = r.placeholder; pos++ < a.length;)if (n = a.charAt(pos - 1), s[t].test(n)) {
                            R[t] = n, i = t;
                            break
                        }
                        if (pos > a.length)break
                    } else R[t] === a.charAt(pos) && t !== u && (pos++, i = t);
                    return e ? b() : u > i + 1 ? (x.val(""), k(0, h)) : (b(), x.val(x.val().substring(0, i + 1))), u ? t : f
                }

                var x = e(this), R = e.map(t.split(""), function (e) {
                    return "?" != e ? l[e] ? r.placeholder : e : void 0
                }), S = x.val();
                x.data(e.mask.dataName, function () {
                    return e.map(R, function (e, t) {
                        return s[t] && e != r.placeholder ? e : null
                    }).join("")
                }), x.attr("readonly") || x.one("unmask", function () {
                    x.unbind(".mask").removeData(e.mask.dataName)
                }).bind("focus.mask", function () {
                    clearTimeout(n);
                    var e;
                    S = x.val(), e = y(), n = setTimeout(function () {
                        b(), e == t.length ? x.caret(0, e) : x.caret(e)
                    }, 10)
                }).bind("blur.mask", function () {
                    y(), x.val() != S && x.change()
                }).bind("keydown.mask", g).bind("keypress.mask", v).bind(a, function () {
                    setTimeout(function () {
                        var e = y(!0);
                        x.caret(e), r.completed && e == x.val().length && r.completed.call(x)
                    }, 0)
                }), y()
            }))
        }
    })
})(jQuery);

$(document).ready(function () {
    var bg_left = $('.bg--left');
    var bg_right = $('.bg--right');
    var slide_start = $('.slide-start');

// после первого клика начинается наведение на левую и правую сторону стартового слайда
    $('.global-overlay').on('click', function () {
        if ($(this).hasClass('no-click')) {
            return false;
        }
        else {
            $(this).remove();
        }
    });

// фикс скролла мышкой при наведении на слайдер и стартовый слайд
    $('.slider-wrap').hover(function () {
        $('.page-wrap').addClass('swipe')
    }, function () {
        $('.page-wrap').removeClass('swipe')
    });

    $('.page-wrap').on('mousewheel', function (event) {
        if (!($('.page-wrap').hasClass('swipe'))) {
            return false;
        }
    });

// стартовый слайд, наведение на половины
    $('.slide-start--left').hover(function () {
        bg_left.removeClass('inhover').addClass('hover');
        slide_start.removeClass('right-hover').addClass('left-hover');
    }, function () {
        bg_left.removeClass('hover');
        slide_start.removeClass('left-hover');
    });

    $('.slide-start--right').hover(function () {
        bg_left.removeClass('hover').addClass('inhover');
        slide_start.removeClass('left-hover').addClass('right-hover');
    }, function () {
        bg_left.removeClass('inhover');
        slide_start.removeClass('right-hover');
    });

// переход со стартового слайда на слайдер
    $('.js--checkout-left').on('click', function () {
        $('.slider-wrap').removeClass('move-right').addClass('move-left');

        setTimeout(function () {
            bg_left.addClass('active');
            $('.slider-wrap').addClass('animated').removeClass('move-left');
            $('.slide-start-test').addClass('move-right');
            $('.fullpage .section').each(function () {
                var nav = $(this).find('.fp-slidesNav ul');
                nav.find('li').eq(0).find('a').trigger('click');
            });
        }, 50);

        setTimeout(function () {
            $('.slide-start-test').removeClass('animated');
        }, 500);

        if ($('.slide1 .img-home').hasClass('img-home-start')) {
            setTimeout(function () {
                slide_home();
            }, 500);
        }

        return false;
    });

    $('.js--checkout-right').on('click', function () {

        $('.slider-wrap').removeClass('move-left').addClass('move-right');

        setTimeout(function () {
            bg_left.addClass('inactive');
            $('.slider-wrap').addClass('animated').removeClass('move-right');
            $('.slide-start-test').addClass('move-left');
            $('.fullpage .section').each(function () {
                var nav = $(this).find('.fp-slidesNav ul');
                var index_active = nav.find('li a.active').closest('li').index();
                nav.find('li').eq(index_active - 1).find('a').trigger('click');
            });
        }, 50);

        setTimeout(function () {
            $('.slide-start-test').removeClass('animated');
        }, 500);

        if ($('.slide1 .img-home').hasClass('img-home-start')) {
            setTimeout(function () {
                slide_home();
            }, 500);
        }

        return false;
    });

// переход на стартовый слайд
    $('.js--show-start').on('click', function () {

        if (bg_left.hasClass('active') || bg_left.hasClass('inactive')) {
            if (bg_left.hasClass('active')) {
                $('.slide-start-test').removeClass('move-left').addClass('move-right');
                setTimeout(function () {
                    $('.slide-start-test').addClass('animated').removeClass('move-right');
                    bg_left.removeClass('active');
                    $('.slider-wrap').addClass('move-left')
                }, 50);
                setTimeout(function () {
                    $('.slider-wrap').removeClass('animated')
                }, 500);
            }
            else {
                $('.slide-start-test').removeClass('move-right').addClass('move-left');
                setTimeout(function () {
                    $('.slide-start-test').addClass('animated').removeClass('move-left');
                    bg_left.removeClass('inactive');
                    $('.slider-wrap').addClass('move-right');
                }, 50);
                setTimeout(function () {
                    $('.slider-wrap').removeClass('animated')
                }, 500);
            }
        }

        return false;
    });

// листать ниже
    $('.js--slide-down').on('click', function () {
        var nav = $('#fp-nav ul');
        var index_active = nav.find('li a.active').closest('li').index();
        nav.find('li').eq(index_active + 1).find('a').trigger('click');
        return false;
    });

// листать слайдер вправо
    $('.js--slide-right').on('click', function () {
        $('.fullpage .section').each(function () {
            var nav = $(this).find('.fp-slidesNav ul');
            var index_active = nav.find('li a.active').closest('li').index();
            nav.find('li').eq(index_active + 1).find('a').trigger('click');
        });
        bg_left.removeClass('active').addClass('inactive');

        // анимация
        if ($('.fullpage .fp-section:nth-child(2)').hasClass('active') && $('.img-podium').hasClass('img-podium-start')) {
            podium_animation();
        }

        return false;
    });
// листать слайдер влево
    $('.js--slide-left').on('click', function () {
        $('.fullpage .section').each(function () {
            var nav = $(this).find('.fp-slidesNav ul');
            var index_active = nav.find('li a.active').closest('li').index();
            nav.find('li').eq(index_active - 1).find('a').trigger('click');
        });
        bg_left.addClass('active').removeClass('inactive');

        // анимация
        if ($('.fullpage .fp-section:nth-child(2)').hasClass('active') && $('.scheme-list--el').hasClass('scheme-list--el-start')) {
            scheme_animation();
        }
        return false;
    });

// оказать корзину
    $('.js--show-cart').on('click', function () {
        $('.popup').toggleClass('popup-hidden');
        $('.header').toggleClass('show-cart');
        if (!$('.popup').hasClass('popup-hidden')) {
            $('.popup-call').addClass('visible');
        }
        else {
            $('.popup-call').removeClass('visible');
            $('.cart-list').removeClass('hidden-block');
            $('.cart-form').addClass('hidden-block');
            $('.js--show-cart-form').removeClass('js--form-submit').addClass('js--show-cart-form');
            $('.popup--cart h2').text('Моя корзина');
        }
        return false;
    });

// показать карту
    $('.js--show-contacts').on('click', function () {
        $('.popup').toggleClass('popup-hidden');
        $('.popup-contacts').addClass('visible');
        return false;
    });

// скрыть корзину и карту
    $('.popup-overlay').on('click', function () {
        $('.popup--content').removeClass('visible');
        $('.popup').addClass('popup-hidden');
        $('.header').removeClass('show-cart');
        $('footer').removeClass('visible');
        $('.cart-list').removeClass('hidden-block');
        $('.cart-form').addClass('hidden-block');
        $('.js--show-cart-form').removeClass('js--form-submit').addClass('js--show-cart-form');
        $('.popup--cart h2').text('Моя корзина');
    });
    $('.js--popup-close').on('click', function () {
        $('.popup--content').removeClass('visible');
        $('.popup').addClass('popup-hidden');
        $('.header').removeClass('show-cart');
        $('footer').removeClass('visible');
        $('.cart-list').removeClass('hidden-block');
        $('.cart-form').addClass('hidden-block');
        $('.js--show-cart-form').removeClass('js--form-submit').addClass('js--show-cart-form');
        $('.popup--cart h2').text('Моя корзина');
        return false;
    });

// переключиться с корзины на форму
    $('.js--show-cart-form').on('click', function () {
        $('.popup--cart h2').text('Отправить заявку');
        $('.cart-list').addClass('hidden-block');
        $('.cart-form').removeClass('hidden-block');
        $('.js--show-cart-form').addClass('js--form-submit').removeClass('js--show-cart-form');
        return false;
    });

    // изменение выбора состава кабинок
    $('.js--calc').on('change', function () {
        var inp = $(this);

        if (!inp.hasClass('js--inner-calc')) {
            if (inp.is(':checked')) {
                inp.closest('.scheme-list--el').addClass('active');
            }
            else {
                inp.closest('.scheme-list--el').removeClass('active');
            }
        }

        var total = Number(inp.closest('.scheme-list').data('sum'));
        var discount = Number(inp.closest('.scheme-list').data('discount'));

        $('.js--calc:checked').each(function () {
            var this_inp = $(this);

            if (!this_inp.hasClass('js--inner-calc')) {

                if (this_inp.hasClass('has-inner-checkbox')) {
                    total = total + Number(this_inp.closest('.scheme-list--el').find('.ref--popup .btns input:checked').data('sum'));
                    discount = discount + Number(this_inp.closest('.scheme-list--el').find('.ref--popup .btns input:checked').data('discount'));
                }
                else {
                    total = total + Number(this_inp.data('sum'));
                    discount = discount + Number(this_inp.data('discount'));
                }
            }
        });

        $('.scheme--price .economy i').text(discount);
        $('.total').text(total);
    });

    // анимация коробки
    $('.box-top-wrap').on('click', function () {
        $('.box-top-wrap').addClass('hidden-box');

        $('.imp-top').addClass('size-top');

        setTimeout(function () {
            $('.box-bottom').addClass('hidden-box');
            $('.imp-top').addClass('hidden-top');
        }, 500)
    });

    // добавить в корзину
    $('.js--add-basket').on('click', function () {
        var btn = $(this);

        var current = {};

        $('.cart-list--el').each(function(){
            var block = $(this).find('input');
            current[block.attr('name')] = block.val();
        });
        current['added'] = btn.data('id');

        $('.header--cart').addClass('active');
        $('.header--cart .items').text('3');

        var method = btn.data('method');
        var action = btn.data('action');

        $.ajax({
            type: method,
            url: action,
            data: current,
            error: function(){
              console.log('error')
            },
            success: function (data) {

                var parse_data = jQuery.parseJSON(data);

                $('.cart-list-wrap').empty('').html(parse_data.html);
                $('.cart-summ').text(parse_data.summ + ' руб.')

                if ($('.cart-list--el').length > 3) {
                    $('.cart-list').owlCarousel({
                        margin: 0,
                        loop: true,
                        autoWidth: false,
                        nav: true,
                        dots: false,
                        navText: [,],
                        autoplay: true,
                        autoplayTimeout: 4000,
                        autoplayHoverPause: true,
                        responsiveClass: true,
                        items: 3
                    });
                }

                $('.header--cart').addClass('active');
                $('.header--cart .items').text(parse_data.total);
            }
        });

        return false;
    });

    $('.js--add-scheme').on('click', function () {
        var form = $(this).closest('.scheme-form');

        var method = form.attr('method');
        var action = form.attr('action');
        var data = form.serialize();

        $.ajax({
            type: method,
            url: action,
            data: data,
            error: function(){
              console.log('error')
            },
            success: function (data) {

                var parse_data = jQuery.parseJSON(data);

                $('.cart-list-wrap').empty('').html(parse_data.html);
                $('.cart-summ').text(parse_data.html + ' руб.')

                if ($('.cart-list--el').length > 3) {
                    $('.cart-list').owlCarousel({
                        margin: 0,
                        loop: true,
                        autoWidth: false,
                        nav: true,
                        dots: false,
                        navText: [,],
                        autoplay: true,
                        autoplayTimeout: 4000,
                        autoplayHoverPause: true,
                        responsiveClass: true,
                        items: 3
                    });
                }

                $('.header--cart').addClass('active');
                $('.header--cart .items').text(parse_data.total);
            }
        });

        return false;
    });

    $(document).on('click', '.js--form-submit', function () {

        var form = $(this).parents('.main-form');
        var errors = false;

        $(form).find('.required').each(function () {
            var val = $(this).prop('value');
            if (val == '') {
                $(this).addClass('error');
                errors = true;
            }
            else {
                if ($(this).hasClass('inp-mail')) {
                    if (validateEmail(val) == false) {
                        $(this).addClass('error');
                        errors = true;
                    }
                }
            }
        });

        if (errors == false) {
            var button_value = $(form).find('.js--form-submit').val();
            $(form).find('.js--form-submit').text('Отправляем...');

            var method = form.attr('method');
            var action = form.attr('action');
            var data = form.serialize();
            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function (data) {
                    $(form).find('.inp').prop('value','');
                    $(form).find('.js--form-ok').trigger('click');
                },
                error: function (data) {
                    $(form).find('.js--form-submit').prop('value', 'Ошибка');
                    setTimeout(function () {
                        $(form).find('.js--form-submit').prop('value', button_value);
                    }, 2000);
                }
            });
        }

        return false;
    });

    $(document).on('focus', '.inp', function () {
        $(this).removeClass('error');
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
});


$(document).ready(function () {
    var bg_left = $('.bg--left');
    var bg_right = $('.bg--right');
    var slide_start = $('.slide-start');
    var parallax_init = false;

    $('.scrollbar-inner').scrollbar();

    $('.promo-slider').owlCarousel({
        margin: 0,
        loop: true,
        autoWidth: false,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        responsiveClass: true,
        items: 1
    });

    if ($('.cart-list--el').length > 3) {
        $('.cart-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            nav: true,
            dots: false,
            navText: [,],
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            responsiveClass: true,
            items: 3
        });
    }

    if ($('.scheme-slider').length) {

        $('.scheme-slider').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            nav: true,
            dots: false,
            navText: [,],
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            responsiveClass: true,
            items: 1
        });
    }

    // вертикальный слайдер
    if ($('.fullpage').length) {
        $('.fullpage').each(function () {
            $(this).fullpage({
                'verticalCentered': false,
                'css3': true,
                'sectionsColor': ['#F0F2F4', '#fff', '#fff', '#fff'],
                'slidesNavigation': true,
                'navigation': true,
                'navigationPosition': 'right',
                'navigationTooltips': ['fullPage.js', 'Powerful', 'Amazing', 'Simple'],

                'afterLoad': function () {
                    start_animation();
                },

                'onLeave': function (index, nextIndex, direction) {
                    if ((index == 1 || index == 3) && nextIndex == 2 && bg_left.hasClass('active') && $('.scheme-list--el').hasClass('scheme-list--el-start')) {
                        scheme_animation();
                    }
                    if ((index == 1 || index == 3) && nextIndex == 2 && bg_left.hasClass('inactive') && $('.img-podium').hasClass('img-podium-start')) {
                        podium_animation();
                    }
                    if (index == 2 && nextIndex == 3 && $('.img-man').hasClass('img-man-start')) {
                        promo_animation();
                    }
                }
            })
        })
    }

    ymaps.ready(function () {
        var myMap2 = new ymaps.Map('map2', {
                center: [54.795238, 32.047323],
                zoom: 14
            }, {
                searchControlProvider: 'yandex#search'
            }),

            myPlacemark2 = new ymaps.Placemark(myMap2.getCenter(), {}, {
                iconLayout: 'default#image',
                iconImageHref: 'img/icon--map.png',
                iconImageSize: [85, 95],
                iconImageOffset: [-25, -95]
            });

        myMap2.geoObjects.add(myPlacemark2);
        myMap2.behaviors.disable('scrollZoom');
    });

    ymaps.ready(function () {
        var myMap = new ymaps.Map('map1', {
                center: [54.795238, 32.047323],
                zoom: 14
            }, {
                searchControlProvider: 'yandex#search'
            }),

            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
                iconLayout: 'default#image',
                iconImageHref: 'img/icon--map.png',
                iconImageSize: [85, 95],
                iconImageOffset: [-25, -95]
            });

        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors.disable('scrollZoom');
    });

    $('.inp-phone').mask('+7(999)999-99-99');
});